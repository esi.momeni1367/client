//
//  Setting.swift
//  AppStore
//
//  Created by Esmaeil on 5/13/23.
//

import Foundation

class Setting {
    
    private static let KEY_TOKEN = "key.fcmToken"
    private static let KEY_USERNAME = "key.username"

    private static var settingInstance : Setting?
    
    var token : String?{
        didSet{
            self.save()
        }
    }
    
    var username : String?{
        didSet{
            self.save()
        }
    }

    static var `default` : Setting{
        get{
            if settingInstance == nil{
                settingInstance = Setting()
            }
            
            return settingInstance!
        }
    }
    
    public func save(){
        if token != nil{
            UserDefaults.standard.set(self.token!, forKey: Setting.KEY_TOKEN)
        }
        if username != nil{
            UserDefaults.standard.set(self.username!, forKey: Setting.KEY_USERNAME)
        }
    }
    
    public func load() -> Bool{
        self.token = UserDefaults.standard.string(forKey: Setting.KEY_TOKEN)
        self.username = UserDefaults.standard.string(forKey: Setting.KEY_USERNAME)

        return true
    }
    
    public func logout(){
        token = ""
        username = ""
    }
    
}
