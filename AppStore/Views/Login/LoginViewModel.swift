//
//  LoginViewModel.swift
//  AppStore
//
//  Created by Esmaeil on 5/8/23.
//

import Foundation
import Combine

class LoginViewModel: ObservableObject {
    

    private var bag = Set<AnyCancellable>()
    
    @Published private (set) var loginRes: LoginModel?
        
}

