//
//  AccountView.swift
//  AppStore
//
//  Created by Esmaeil on 5/16/23.
//

import SwiftUI

struct AccountView: View {
        
    @State var apps: [ApplicationViewModel] = []
    @State var settingIsShowing: Bool = false

    @Binding var currentPage: PageStatus
    @Binding var previusePage: PageStatus
    @Binding var globalData: GlobalData
    
    var body: some View {
        VStack(alignment: .trailing, spacing: 0) {
            
            Spacer()

            HStack {
                Spacer()
                VStack {
                    Text("تنظیمات")
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                }
                .frame(minWidth: 120, minHeight: 40)
                .background(Color.gray.gradient)
                .cornerRadius(10)
                .onTapGesture {
                    settingIsShowing.toggle()
                }
                .padding(12)

                Text("حساب کاربری")
                    .font(.MacStore.HomePage.featuredAppMainTitle)
                    .fontWeight(.bold)
                    .padding(.horizontal)
            
            }

            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center, spacing: 0) {
                    HStack {
                        ForEach(apps , id:\.id) { appInfo in
                            AppCardView(app: appInfo)
                                .onTapGesture {
                                    self.currentPage = .AppDetail
                                    self.previusePage = .account
                                    self.globalData.appDetail = appInfo
                                }
                        }
                    }
                }
                .padding(.horizontal)
                .padding(.bottom, 40)
            }.padding(.top)
        }
        .sheet(isPresented: self.$settingIsShowing) {
            SettingView()
                .environmentObject(AFManager())
        }

    }
}

