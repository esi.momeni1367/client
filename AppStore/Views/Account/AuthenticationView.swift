//
//  AuthenticationView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/5/23.
//

import SwiftUI

struct AuthenticationView: View {
    
    private let afManager = AFManager()

    @Environment(\.presentationMode) var presentationMode

    @State var mobile: String = ""
    @State var keyStr: String = ""
    @State var enterCode: Bool = false
    @Binding var username : String

    
    var body: some View {
        VStack{
            Image("AppsIcon").padding(10)
            Text("به مک استور خوش آمدید")
                .fontWeight(.bold)
                .padding(10)
            
            Text("به حساب کاربری خود وارد شوید تا به دریایی از نرم‌افزارهای مک دسترسی داشته باشید.")
                .padding(.horizontal, 10)
            

            if enterCode {
                HStack{
                    SecureField("", text: self.$keyStr)
                        .frame(width: 100, height: 20)
                       .textFieldStyle(PlainTextFieldStyle())
                       .padding(10)
                       .background(RoundedRectangle(cornerRadius: 2).stroke(Color.gray))

                    Text("کلمه عبور")
                        .fontWeight(.bold)
                        .padding(10)
                }.padding(.top, 5)
                
                VStack {
                    Text("ورود")
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                }
                .frame(minWidth: 120, minHeight: 40)
                .background(Color.accentColor)
                .cornerRadius(10)
                .onTapGesture {
                    let otpKeyData = OtpKeyData()
                    otpKeyData.otp?.mobile = mobile
                    otpKeyData.otp?.key = keyStr
                    afManager.login(otpKeyData: otpKeyData) { status, info in
                        print(info.username ?? "?")
                        print(info.token ?? "!")
                        Setting.default.username = info.username
                        Setting.default.token = info.token
                        username = info.username ?? ""
                        presentationMode.wrappedValue.dismiss()
                    }
                }
                .padding(12)

            }else{
                HStack{
                    TextField("", text: self.$mobile)
                        .frame(width: 100, height: 20)
                       .textFieldStyle(PlainTextFieldStyle())
                       .padding(10)
                       .background(RoundedRectangle(cornerRadius: 2).stroke(Color.gray))
                    
                    Text("شماره موبایل")
                        .fontWeight(.bold)
                        .padding(10)

                }.padding(.top, 5)

                VStack {
                    Text("دریافت کد")
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                }
                .frame(minWidth: 120, minHeight: 40)
                .background(Color.accentColor)
                .cornerRadius(10)
                .onTapGesture {
                    let otpData = OtpData()
                    otpData.otp?.mobile = mobile
                    afManager.otp(otpData: otpData) { status, info in
                        print(info.status ?? "---")
                        enterCode = true
                    }
                }
                .padding(12)
            }
        }

    }
}

