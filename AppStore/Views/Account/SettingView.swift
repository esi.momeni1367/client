//
//  SettingView.swift
//  AppStore
//
//  Created by Esmaeil on 5/16/23.
//

import SwiftUI

struct SettingView: View {
    
    private let afManager = AFManager()

    @Environment(\.presentationMode) var presentationMode
    @State var username: String = ""

    var body: some View {
        
        VStack{
            Image("AppsIcon").padding(10)
            Text("اطلاعات کاربر")
                .fontWeight(.bold)
                .padding(10)
                    
            HStack{
                TextField(Setting.default.username ?? "", text: self.$username)
                    .frame(width: 100, height: 20)
                   .textFieldStyle(PlainTextFieldStyle())
                   .padding(10)
                   .background(RoundedRectangle(cornerRadius: 2).stroke(Color.gray))

                Text("نام کاربری")
                    .fontWeight(.bold)
                    .padding(10)
            }.padding(.top, 5)
            
            HStack{
                Text(Setting.default.username ?? "")
                    .fontWeight(.light)
                    .padding(10)

                Text("شماره موبایل")
                    .fontWeight(.bold)
                    .padding(10)
            }
            .padding(.top, 5)
            .padding(.bottom, 10)

            HStack{
                VStack {
                    Text("بستن")
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                }
                .frame(minWidth: 80, minHeight: 30)
                .background(Color.gray.gradient)
                .cornerRadius(10)
                .onTapGesture {
                    presentationMode.wrappedValue.dismiss()
                }
                .padding(12)

                VStack {
                    Text("ذخیره")
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                }
                .frame(minWidth: 80, minHeight: 30)
                .background(Color.accentColor)
                .cornerRadius(10)
                .onTapGesture {
                    presentationMode.wrappedValue.dismiss()
                }
                .padding(12)
            }
        }
        .padding(.horizontal, 50)

    }
        
}

