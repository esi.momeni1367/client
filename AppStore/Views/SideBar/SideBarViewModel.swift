//
//  SideBarViewModel.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//


import Foundation
import SwiftUI
import Combine

final class SideBarViewModel: ObservableObject {
    
    private let apiManager = APIManager<SideBarEndpoint>()
    
    private var bag = Set<AnyCancellable>()
    
    @Published private (set) var sidebarList: [SideBarItemViewModel]?
    @Published private(set) var currentSideBarId: String = ""
    
    func getSideBar() {
        
        apiManager.getData(from: .getSideBar)
            .decode(type: SideBarContainer.self, decoder: Container.jsonDecoder)
            .map { $0 }
            .replaceError(with: SideBarContainer(version: "", tags: []))
            .receive(on: RunLoop.main)
            .sink(receiveValue: { [weak self] sidebarInfo in
                self?.sidebarList = sidebarInfo.tags.compactMap({ SideBar in
                    SideBarItemViewModel(sideBar: SideBar)
                })
                self?.sidebarList?.first(where: {$0.title == "خانه"})?.isSelected = true
            })
            .store(in: &bag)
    }
    
    func updateSideBarItem(id: String) {
        self.currentSideBarId = id
    }
}


class SideBarItemViewModel {
    
    var sideBar: SideBarModel
    
    var version: String {
        return self.sideBar.version
    }
    
    var title: String {
        return self.sideBar.title
    }
    
    var description: String {
        return self.sideBar.desc
    }
    
    var icon: String {
        return self.sideBar.image_url
    }
    
    var showInSidebar: Bool {
        return self.sideBar.show_in_sidebar
    }
    
    var createdAt: String {
        return self.sideBar.created_at
    }
    
    var updatedAt: String {
        return self.sideBar.updated_at
    }
    
    var id: String {
        return self.sideBar._id
    }
    
    var isSelected: Bool = false
    
    
    init(sideBar: SideBarModel?) {
        self.sideBar = sideBar ?? SideBarModel(version: "", title: "", desc: "", image_url: "", show_in_sidebar: false, created_at: "", updated_at: "", _id: "")
    }
}
