//
//  SideBarItemView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import SwiftUI
import Kingfisher

struct SideBarItemView: View {
    
    @State var sidebarItem: SideBarItemViewModel
    
    var body: some View {
        HStack {
            Spacer()
            Text(self.sidebarItem.title)
                .font(.MacStore.General.sideBarItem)
                .multilineTextAlignment(.trailing)
                

            KFImage(URL(string: self.sidebarItem.icon))
                .resizable()
                .frame(width: 40, height: 40, alignment: .center)
                .padding(.trailing)
                
        }
        .frame(width: 220, height: 40, alignment: .center)
        

    }
}
