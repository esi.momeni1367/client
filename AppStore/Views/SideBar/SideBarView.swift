//
//  SideBarView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/1/23.
//

import SwiftUI

struct SideBarView: View {
    
    @State private var selectedItem: String?
    
    @Binding var currentPage: PageStatus
    @Binding var previusePage: PageStatus
    @State var authIsShowing: Bool = false
    
    @EnvironmentObject var sideBarViewModel: SideBarViewModel
    
    @State var username = Setting.default.username ?? ""

    var body: some View {
        
        VStack {
            NavigationBarView()
            
            SearchBarView()
                .padding(.all)
            
            List {
                ForEach(self.sideBarViewModel.sidebarList ?? [], id:\.id) { sideBarItem in
                    if sideBarItem.title == "خانه" {
                        
                    }
                    if self.selectedItem == sideBarItem.id {
                        SideBarItemView(sidebarItem: sideBarItem)
                            .background(
                                .ultraThinMaterial,
                                in: RoundedRectangle(cornerRadius: 8, style: .continuous)
                            )
                            .frame(width: 220, height: 40, alignment: .center)
                            .onTapGesture {
                                self.selectedItem = sideBarItem.id
                                self.sideBarViewModel.updateSideBarItem(id: sideBarItem.id)
                                self.previusePage = self.currentPage
                                if sideBarItem.title == "خانه" {
                                    self.currentPage = .Home
                                }else {
                                    self.currentPage = .AppList
                                }
                                
                            }
                    }else {
                        SideBarItemView(sidebarItem: sideBarItem)
                            .background(.gray.opacity(0.00001),
                                        in: RoundedRectangle(cornerRadius: 8, style: .continuous))
                            .frame(width: 220, height: 40, alignment: .center)
                            .onTapGesture {
                                self.selectedItem = sideBarItem.id
                                self.sideBarViewModel.updateSideBarItem(id: sideBarItem.id)
                                self.previusePage = self.currentPage
                                if sideBarItem.title == "خانه" {
                                    self.currentPage = .Home
                                }else {
                                    self.currentPage = .AppList
                                }
                            }
                    }
                    
                }
            }
            .scrollContentBackground(.hidden)
            
            
            Button {
                if username == "" {
                    self.authIsShowing.toggle()
                }else{
                    self.previusePage = self.currentPage
                    self.currentPage = .account
                }
            } label: {

                Text(username == "" ? "ورود" : username)
            }
            
            Spacer(minLength: 15)
            
            if !username.isEmpty {
                Button {
                    Setting.default.logout()
                    username = ""
                } label: {

                    Text("خروج")
                }

            }
            
            Spacer(minLength: 20)

        }
        .sheet(isPresented: self.$authIsShowing) {
            AuthenticationView(username: $username)
                .environmentObject(AFManager())
        }
        .background(VisualEffectView().ignoresSafeArea())
        .frame(minWidth: 250,idealWidth: 250, maxWidth: 250)
        .onAppear {
            if !(self.sideBarViewModel.sidebarList?.count ?? 0 > 0) {
                self.sideBarViewModel.getSideBar()
            }else {
                if self.selectedItem == "" {
                    let home = self.sideBarViewModel.sidebarList?.first(where: {$0.title == "خانه"})
                    self.selectedItem = home?.id
                }
            }
        }
        
        
        
    }
    
    
    func toggleSideBar(selectedItem: String) {
        for i in 0..<(self.sideBarViewModel.sidebarList?.count ?? 0) {
            if self.sideBarViewModel.sidebarList?[i].id == selectedItem {
                self.sideBarViewModel.sidebarList?[i].isSelected = true
            }else {
                self.sideBarViewModel.sidebarList?[i].isSelected = false
            }
        }
    }
}


struct VisualEffectView: NSViewRepresentable {
    func makeNSView(context: Context) -> NSVisualEffectView {
        let effectView = NSVisualEffectView()
        effectView.state = .active
        return effectView
    }

    func updateNSView(_ nsView: NSVisualEffectView, context: Context) {
    }
}


