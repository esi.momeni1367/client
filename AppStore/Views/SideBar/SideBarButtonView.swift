//
//  SideBarButtonView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import SwiftUI

struct SideBarButtonView: View {
    var body: some View {
        HStack {
            Spacer()
            Text("خانه")
                .multilineTextAlignment(.trailing)
            Image(systemName: "star.fill")
                
        }.onTapGesture {
        }
    }
}

struct SideBarButtonView_Previews: PreviewProvider {
    static var previews: some View {
        SideBarButtonView()
    }
}
