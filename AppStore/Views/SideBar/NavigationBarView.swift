//
//  NavigationBarView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/4/23.
//

import SwiftUI

struct NavigationBarView: View {
    
    @State var isShow = false
    
    var body: some View {
        HStack(spacing: 8) {
            Spacer()
            Button {
                NSApp.mainWindow?.toggleFullScreen(self)
            } label: {
                ZStack {
                    Image("FullScreen")
                        .resizable()
                        .frame(width: 15, height: 15)
                        .opacity(self.isShow ? 1 : 0)
                    Circle()
                        .fill(Color(red: 97/255, green: 197/255, blue: 85/255))
                        .frame(width: 15, height: 15)
                        .opacity(self.isShow ? 0 : 1)
                }
            }.buttonStyle(PlainButtonStyle())

            Button {
                NSApp.mainWindow?.miniaturize(self)
            } label: {
                ZStack {
                    Image("Minimize")
                        .resizable()
                        .frame(width: 15, height: 15)
                        .opacity(self.isShow ? 1 : 0)
                    
                    Circle()
                        .fill(Color(red: 244/255, green: 190/255, blue: 79/255))
                        .frame(width: 15, height: 15)
                        .opacity(self.isShow ? 0 : 1)
                }
            }.buttonStyle(PlainButtonStyle())

            
            Button {
                NSApp.mainWindow?.close()
            } label: {
                
                ZStack {
                    Image("Close")
                        .resizable()
                        .frame(width: 15, height: 15)
                        .opacity(self.isShow ? 1 : 0)
                    
                    Circle()
                        .fill(Color(red: 236/255, green: 106/255, blue: 95/255))
                        .frame(width: 15, height: 15)
                        .opacity(self.isShow ? 0 : 1)
                }
            }.buttonStyle(PlainButtonStyle())

            
        }.onHover { over in
            self.isShow = over
        }
        .padding(.trailing)
    }
}

struct NavigationBarView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationBarView()
    }
}


//            NSApp.mainWindow?.toggleFullScreen(self)
//            NSApp.mainWindow?.close()
//            NSApp.mainWindow?.miniaturize(self)
