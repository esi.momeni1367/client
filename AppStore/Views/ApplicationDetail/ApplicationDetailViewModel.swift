//
//  ApplicationDetailViewModel.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/3/23.
//

import Foundation
import Combine

class ApplicationDetailViewModel: ObservableObject {
    
    private let apiManager = APIManager<ApplicationDetailEndpoint>()
    
    private var bag = Set<AnyCancellable>()
    
    @Published private (set) var applicationInfo: ApplicationViewModel?
    
    func retrieveAppById(appId: String) {
                
        apiManager.getData(from: .getApplication(appId: appId))
            .decode(type: ApplicationModel.self, decoder: Container.jsonDecoder)
            .map { $0 }
            .replaceError(with: ApplicationModel(version: "", name: "", logo: "", desc: ApplicationDescriptionContainer(short: ApplicationDescription(en: "", fa: ""), long: ApplicationDescription(en: "", fa: "")), age: "", rank: "", developer: "", catalogue: [], created_at: "", updated_at: "", _id: "", category: ApplicationCategory(version: "", title: "", desc: "", image_url: "", created_at: "", updated_at: "", _id: "")))
            .receive(on: RunLoop.main)
            .sink(receiveValue: { [weak self] appInfo in
                self?.applicationInfo = ApplicationViewModel(appInfo: appInfo)
            })
            .store(in: &bag)
    }
}


class ApplicationViewModel {
    private var appInfo: ApplicationModel
    
    var version: String {
        return self.appInfo.version
    }
    
    var name: String {
        return self.appInfo.name
    }
    
    var logo: String {
        return self.appInfo.logo
    }
    
    var description: ApplicationDescriptionContainer {
        return self.appInfo.desc
    }
    
    var age: String {
        return self.appInfo.age
    }
    
    var rank: String {
        return self.appInfo.rank
    }
    
    var developer: String {
        return self.appInfo.developer
    }
    
    var catalogue: [ApplicationCatalogue] {
        return self.appInfo.catalogue
    }
    
    var OS: [String] {
        return self.appInfo.catalogue.first?.compatibility.os.arch ?? ["MacOS"]
    }
    
    var createdAt: String {
        return self.appInfo.created_at
    }
    
    var updatedAt: String {
        return self.appInfo.updated_at
    }
    
    var id: String {
        return self.appInfo._id
    }
    
    var category: ApplicationCategory {
        return self.appInfo.category
    }
    
    init(appInfo: ApplicationModel?) {
        self.appInfo = appInfo ?? ApplicationModel(version: "", name: "", logo: "", desc: ApplicationDescriptionContainer(short: ApplicationDescription(en: "", fa: ""), long: ApplicationDescription(en: "", fa: "")), age: "", rank: "", developer: "", catalogue: [], created_at: "", updated_at: "", _id: "", category: ApplicationCategory(version: "", title: "", desc: "", image_url: "", created_at: "", updated_at: "", _id: ""))
    }
}
