//
//  ApplicationDetailView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/3/23.
//

import SwiftUI
import Kingfisher

struct ApplicationDetailView: View {
    
    @Binding var currentPage: PageStatus
    @Binding var previusePage: PageStatus
    @Binding var globalData: GlobalData
    
    var body: some View {
        ScrollView {
            VStack {
                ApplicationDetailTopButtonView(currentPage: self.$currentPage, previusePage: self.$previusePage)
                ApplicationDetailTitleView(globalData: self.$globalData)
                Divider()
                    .padding(.horizontal)
                ApplicationSummeryView(globalData: self.$globalData)
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(alignment: .center, spacing: 0) {
                        HStack {
                            ForEach(self.globalData.appDetail.catalogue.first?.screenshots ?? [], id:\.self) { imgUrl in
                                KFImage(URL(string: imgUrl))
                            }
                        }
                    }
                    .padding(.horizontal)
                    .padding(.bottom, 40)
                }.padding(.top)
                Text(self.globalData.appDetail.description.long.fa)
                    .lineLimit(nil)
                    .multilineTextAlignment(.trailing)
                    .padding()
                    
                
                    
            }
        }
    }
}
