//
//  ApplicationDetailTitleView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/3/23.
//

import SwiftUI
import Kingfisher

struct ApplicationDetailTitleView: View {
    
    @Binding var globalData: GlobalData
    
    var body: some View {
        HStack {
            VStack {
                HStack {
                    Spacer()
                    Text(self.globalData.appDetail.name)
                        .multilineTextAlignment(.trailing)
                }
                .padding()
                
                Text(self.globalData.appDetail.description.short.fa)
                    .multilineTextAlignment(.trailing)
                    .padding()
                
                HStack {
                    Spacer()
                    DownloadButton(isHomePage: false, appId: self.globalData.appDetail.id, appName: self.globalData.appDetail.name)
                        .padding()
                }
            }
//            KFImage(URL(string: ""))
            Image("AppsIcon")
        }.padding()
    }
}
