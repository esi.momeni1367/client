//
//  ApplicationDetailTopButtonView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/3/23.
//

import SwiftUI

struct ApplicationDetailTopButtonView: View {
    
    @Binding var currentPage: PageStatus
    @Binding var previusePage: PageStatus
    
    @State var backButtonBackground: Bool = false
    @State var shareButtonBackground: Bool = false
    
    var body: some View {
        HStack{
            ZStack {
                if self.shareButtonBackground {
                    RoundedRectangle(cornerRadius: 5)
                        .fill(Color.gray.opacity(0.2))
                        .frame(width: 30, height: 25, alignment: .center)
                        .padding(.leading)
                }
                Button {
                } label: {
                    Image("Share")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(.gray)
                        .frame(width: 20, height: 20, alignment: .center)
                }
                .buttonStyle(PlainButtonStyle())
                .padding(.leading)
                .onHover { over in
                    self.shareButtonBackground = over
                }
            }.frame(width: 30, height: 25, alignment: .center)
            
            Spacer()
            ZStack {
                if self.backButtonBackground {
                    RoundedRectangle(cornerRadius: 5)
                        .fill(Color.gray.opacity(0.2))
                        .frame(width: 30, height: 25, alignment: .center)
                        .padding(.trailing)
                }
                Button {
                    self.currentPage = self.previusePage
                } label: {
                    Image("RightArrow")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(.gray)
                        .frame(width: 20, height: 20, alignment: .center)
                }
                .buttonStyle(PlainButtonStyle())
                .padding(.trailing)
                .onHover { over in
                    self.backButtonBackground = over
                }
            }.frame(width: 30, height: 25, alignment: .center)
        }
    }
}
