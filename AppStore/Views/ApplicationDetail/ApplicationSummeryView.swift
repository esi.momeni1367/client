//
//  ApplicationSummeryView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/4/23.
//

import SwiftUI

struct ApplicationSummeryView: View {
    
    @Binding var globalData: GlobalData

    
    var body: some View {
        HStack {
            
            ApplicationSummeryItemView(top: "حجم فایل", middle: self.globalData.appDetail.catalogue.first?.size ?? "0.0", bottom: "MB")
            
            
            ApplicationSummeryItemView(top: "سن", middle: self.globalData.appDetail.age, bottom: "")
            
            
            VStack {
                Text("توسعه دهنده")
                    .padding(.horizontal)
                    .padding(.top, 10)
                Image("Person")
                Text(self.globalData.appDetail.developer)
            }
            
            Divider()
                .padding(.vertical)
            
            
            ApplicationSummeryItemView(top: "زبان", middle: self.globalData.appDetail.catalogue.first?.languages.first ?? "EN", bottom: "+\((self.globalData.appDetail.catalogue.first?.languages.count ?? 1) - 1) more")
            
            
            ApplicationSummeryItemView(top: "برای سیستم", middle: self.globalData.appDetail.OS.first ?? "MacOs", bottom: self.globalData.appDetail.OS.last ?? "")
            
            
            ApplicationSummeryItemView(top: "نسخه", middle: self.globalData.appDetail.version, bottom: "")
        }
    }
}




struct ApplicationSummeryItemView: View {
    
    var top: String
    var middle: String
    var bottom: String
    
    var body: some View {
        
        HStack {
            VStack {
                Text(top)
                    .padding(.horizontal)
                    .padding(.top, 10)
                Text(middle)
                    .padding(.horizontal)
                    .padding(.top, 10)
                if !self.bottom.isEmpty {
                    Text(bottom)
                        .padding(.horizontal)
                        .padding(.top, 10)
                }
            }
            .padding()
            if top != "نسخه" {
                Divider()
                    .padding(.vertical)
            }
        }
    }
}
