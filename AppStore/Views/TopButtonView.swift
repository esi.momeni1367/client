//
//  TopButtonView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/4/23.
//

import SwiftUI

struct TopButtonView: View {
    
    @Binding var currentPage: PageStatus
    @Binding var previusePage: PageStatus
    
    @State var backButtonBackground: Bool = false
    
    var body: some View {
        HStack{
            Spacer()
            ZStack {
                if self.backButtonBackground {
                    RoundedRectangle(cornerRadius: 5)
                        .fill(Color.gray.opacity(0.2))
                        .frame(width: 30, height: 25, alignment: .center)
                        .padding(.trailing)
                }
                Button {
                    self.currentPage = self.previusePage
                } label: {
                    Image("RightArrow")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(.gray)
                        .frame(width: 20, height: 20, alignment: .center)
                }
                .buttonStyle(PlainButtonStyle())
                .padding(.trailing)
                .onHover { over in
                    self.backButtonBackground = over
                }
            }.frame(width: 30, height: 25, alignment: .center)
        }
    }
}
