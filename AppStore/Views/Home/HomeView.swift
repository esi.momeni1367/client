//
//  HomeView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/1/23.
//

import SwiftUI

struct HomeView: View {
    
    
    @EnvironmentObject var bannersViewModel: BannersViewModel
    @EnvironmentObject var appsViewModel: ApplicationsViewModel

    @Binding var currentPage: PageStatus
    @Binding var previusePage: PageStatus
    @Binding var globalData: GlobalData
    
    
    var body: some View {
        NavigationStack {
            ScrollView(showsIndicators: false) {
                BannerHeader(bannerUrl: self.bannersViewModel.banner?.items.last?.image_url ?? "")
                
                FeaturedAppView(title: "برنامه های ویژه", features: self.bannersViewModel.middleBanner ?? [])
                
                GreatNewAppsView(currentPage: self.$currentPage, previusePage: self.$previusePage, globalData: self.$globalData, appList: self.appsViewModel.applicationList?.apps ?? [])
                    
                Spacer(minLength: 100)
            }
            .padding(.top, -30)
            
        }
    }
}



enum DestinationViews {
    case ApplicationDetail
}
