//
//  BannerHeader.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 2/24/23.
//

import SwiftUI
import Kingfisher

struct BannerHeader: View {
    

    var bannerUrl: String = "banner"
    
    var body: some View {
        KFImage(URL(string: bannerUrl))
            .resizable()
            .aspectRatio(contentMode: .fit)
            .padding([.trailing, .leading, .bottom])
    }
}

struct BannerHeader_Previews: PreviewProvider {
    static var previews: some View {
        BannerHeader()
    }
}
