//
//  FeaturedAppView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/1/23.
//

import SwiftUI
import Kingfisher

struct FeaturedAppView: View {
    
    let title: String
    let features: [BannerViewModel]
    
    var body: some View {
        VStack(alignment: .trailing, spacing: 16) {
            Text(title)
                .font(.MacStore.HomePage.featuredAppTitle)
                .fontWeight(.bold)
                .padding(.horizontal)
            
            HStack(alignment: .center, spacing: 16) {
                ForEach(self.features, id:\.id) { feature in
                    FeatureCardView(feature: feature)
                }
            }
            .padding(.horizontal)
        }
    }
}

struct FeatureCardView: View {
    let feature: BannerViewModel
    
    var body: some View {
        ZStack {
            KFImage(URL(string: feature.imageUrl))
                .resizable()
                .aspectRatio(contentMode: .fit)
            HStack(alignment: .center, spacing: 8) {
                Spacer()
                VStack(alignment: .trailing) {
                    Text(feature.slug)
                        .font(.MacStore.HomePage.featuredAppslug)
                        .fontWeight(.heavy)
                        .foregroundColor(.gray)
                        .padding(.top)
                        .padding(.trailing, 15)
                        
                        
                    Text(feature.title)
                        .font(.MacStore.HomePage.featuredAppTitle)
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                        .padding(.trailing, 20)
                    
                    Text(feature.description)
                        .multilineTextAlignment(.trailing)
                        .font(.MacStore.HomePage.featuredAppdescription)
                        .foregroundColor(.secondary)
                        .lineLimit(3)
                        .padding(.trailing, 20)
                    Spacer()
                        
                }.padding(.leading)
            }
        }
    }
}
