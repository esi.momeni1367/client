//
//  BannerViewModel.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import Foundation
import SwiftUI
import Combine

final class BannersViewModel: ObservableObject {
    
    private let apiManager = APIManager<BannersEndpoint>()
    
    private var bag = Set<AnyCancellable>()
    
    @Published private (set) var banner: BannerContainer?
    @Published private (set) var topBanner: BannerViewModel?
    @Published private (set) var middleBanner: [BannerViewModel]?
    
    func getBanners(skip: Int, limit: Int) {
                
        apiManager.getData(from: .getBanners(skip: skip, limit: limit))
            .decode(type: BannerContainer.self, decoder: Container.jsonDecoder)
            .map { $0 }
            .replaceError(with: BannerContainer(items: [], total: 0, limit: 0, offset: 0))
            .receive(on: RunLoop.main)
            .sink(receiveValue: { [weak self] bannersInfo in
                self?.banner = bannersInfo
                self?.topBanner = BannerViewModel(banner: bannersInfo.items.first(where: {$0.type == "full-width"}))
                self?.middleBanner = bannersInfo.items.map({BannerViewModel(banner: $0)})
                self?.middleBanner?.removeAll(where: {$0.type != "half-width"})
            })
            .store(in: &bag)
    }
}


class BannerViewModel {
    var banner: BannerModel
    
    var version: String {
        return self.banner.version
    }
    
    var title: String {
        return self.banner.title
    }
    
    var slug: String {
        return self.banner.slug
    }
    
    var type: String {
        return self.banner.type
    }
    
    var description: String {
        return self.banner.desc
    }
    
    var imageUrl: String {
        return self.banner.image_url
    }
    
    var tagId: String {
        return self.banner.tag_id
    }
    
    var createdAt: String {
        return self.banner.created_at
    }
    
    var updatedAt: String {
        return self.banner.updated_at
    }
    
    var id: String {
        return self.banner._id
    }
    
    
    
    init(banner: BannerModel?) {
        self.banner = banner ?? BannerModel(version: "", title: "", slug: "", type: "", desc: "", image_url: "", tag_id: "", created_at: "", updated_at: "", _id: "")
    }
}
