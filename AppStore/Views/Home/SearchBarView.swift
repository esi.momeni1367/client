//
//  SearchBarView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 2/24/23.
//

import SwiftUI


struct SearchBarView: View {
    
    @State private var searchText = ""
    
    var body: some View {
        HStack {
            Image(systemName: "magnifyingglass")
                .foregroundColor(.gray)
            TextField("جستجو", text: $searchText)
                .textFieldStyle(RoundedBorderTextFieldStyle())
        }
    }
}

struct SearchBarView_Previews: PreviewProvider {
    static var previews: some View {
        SearchBarView()
    }
}
