//
//  ApplicationsViewModel.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/3/23.
//

import Foundation
import Combine

class ApplicationsViewModel: ObservableObject {
    
    private let apiManager = APIManager<ApplicationsEndpoint>()
    
    private var bag = Set<AnyCancellable>()
    
    @Published private (set) var applicationList: ApplicationListViewModel?
    
    func getApps(skip: Int, limit: Int) {
        
        apiManager.getData(from: .getApplications(skip: skip, limit: limit))
            .decode(type: ApplicationListContainer.self, decoder: Container.jsonDecoder)
            .map { $0 }
            .replaceError(with: ApplicationListContainer(items: [], total: 0, limit: 0, offset: 0))
            .receive(on: RunLoop.main)
            .sink(receiveValue: { [weak self] appList in
                self?.applicationList = ApplicationListViewModel(appList: appList)
            })
            .store(in: &bag)
    }
}

class ApplicationListViewModel {
    private var appList: ApplicationListContainer
    
    var apps: [ApplicationViewModel] {
        return self.appList.items.map({ApplicationViewModel(appInfo: $0)})
    }
    
    var total: Int {
        return self.appList.total
    }
    
    var limit: Int {
        return self.appList.limit
    }
    
    var offset: Int {
        return self.appList.offset
    }
    
    
    init(appList: ApplicationListContainer?) {
        self.appList = appList ?? ApplicationListContainer(items: [], total: 0, limit: 0, offset: 0)
    }
}
