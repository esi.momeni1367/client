//
//  GreatNewAppsView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/1/23.
//

import SwiftUI
import Kingfisher
import Alamofire

struct GreatNewAppsView: View {
    
    
    @Binding var currentPage: PageStatus
    @Binding var previusePage: PageStatus
    @Binding var globalData: GlobalData
    
    var appList: [ApplicationViewModel]
    
    var body: some View {
        VStack(alignment: .trailing, spacing: 0) {
            HStack {
                Button {
                    self.previusePage = self.currentPage
                    self.currentPage = .GreatAppList
                } label: {
                    Text("بیشتر")
                        .foregroundColor(.blue)
                        .font(.MacStore.General.downloadButton)
                }
                .buttonStyle(PlainButtonStyle())
                .padding(.leading)

                Spacer()
                Text("بهترین اپ‌ها و آپدیت‌های تازه")
                    .font(.MacStore.HomePage.featuredAppMainTitle)
                    .fontWeight(.bold)
                    .padding(.horizontal)
            }
            
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center, spacing: 0) {
                    HStack {
                        ForEach(appList, id:\.id) { appInfo in
                            AppCardView(app: appInfo)
                                .onTapGesture {
                                    self.currentPage = .AppDetail
                                    self.globalData.appDetail = appInfo
                                }
                        }
                    }
                }
                .padding(.horizontal)
                .padding(.bottom, 40)
            }.padding(.top)
        }
    }
}

struct AppCardView: View {
    let app: ApplicationViewModel
    
    var body: some View {
        HStack(spacing: 0) {
            
//            HStack(spacing: 4) {
//                ForEach(0..<app.rating) { _ in
//                    Image(systemName: "star.fill")
//                        .foregroundColor(.yellow)
//                }
//                ForEach(0..<5 - app.rating) { _ in
//                    Image(systemName: "star")
//                        .foregroundColor(.yellow)
//                }
//            }
//            .font(.caption)
//            .padding(.top, 4)
            
            
            DownloadButton(isHomePage: true, appId: app.id, appName: app.name)
                .environmentObject(AFManager())


            VStack(alignment: .trailing) {
                HStack {
                    Spacer()
                    Text(app.name)
                        .font(.MacStore.AppsList.appTitle)
                        .fontWeight(.bold)
                        .multilineTextAlignment(.trailing)
                }
                
                HStack {
                    Spacer()
                    Text(app.category.title)
                        .font(.MacStore.AppsList.appDescription)
                        .foregroundColor(.secondary)
                        .multilineTextAlignment(.trailing)
                }
            }
            .frame(width: 150)
            .padding(.trailing, 8)
            
            KFImage(URL(string: app.logo))
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
                
        }
        .frame(width: 300)
    }
}


struct DownloadButton: View {
    
    private let afManager = AFManager()

    @State private var downloadProgress: Double = 0.0
    @State private var isDownloading = false
    @State var appExisted = false

    @State var isHomePage: Bool = true
    @State var authIsShowing: Bool = false

    var appId: String
    var appName: String

    var body: some View {
    
        if !appExisted {
            if isDownloading {
                ProgressView(value: downloadProgress)
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                    .frame(width: 16, height: 16)
            } else {
                Image(systemName: "arrow.down.circle.fill")
                    .font(.system(size: 24))
                    .foregroundColor(.accentColor)
            }
        }

//        Text(isDownloading ? "Downloading..." : "Get")
//            .fontWeight(.semibold)
//            .foregroundColor(.white)
//            .padding(.leading, 4)

        
        Button(action: {
            let token = Setting.default.token ?? ""

            if token == "" {
                self.authIsShowing.toggle()
            }else{
                // Start downloading
                
                if appExisted {
                    openApplication(appName: "CleanMyMac X")
                }else{
                    afManager.getAppInfo(appId: appId) { status, appInfo in
                        
                        if status && !appInfo.bundle.isEmpty && !appInfo.script.isEmpty {
                            isDownloading = true

                            downloadDmgFile(dmgFileUrl: appInfo.bundle) { status in
                                print(">>>>> \(status)")
                                isDownloading = false
                                installAppOnMac(scriptFileName: "dd.scpt") { status in
                                    if status {
                                        let _ = deleteFile(fileName: "dd.scpt")
                                        let _ = deleteFile(fileName: "bundle.dmg")
                                        if existApplication(appName: "CleanMyMac X") {
                                            appExisted = true
                                        }
                                    }
                                }
                            }
                            
                            afManager.downloadScriptFile(scriptFileUrl: appInfo.script) { status in
                                print("------ \(status)")
                            }
                        }
                    }
                }
                
            }

        }) {
            ZStack {
                Capsule(style: .circular)
                    .fill(self.isHomePage ? Color.white : Color.blue)
                    .frame(width: 60, height: 25, alignment: .center)
                    
                Text(!appExisted ? "دانلود" : "باز کن")
                    .foregroundColor(self.isHomePage ? Color.blue : Color.white)
                    .font(.MacStore.General.downloadButton)
            }
            .padding(.horizontal, 16)
            .padding(.vertical, 10)
            .background(Color.clear)
            .cornerRadius(8)
            .onAppear(){
                print(">>>>>> \(appName)")
                if appName == "CleanMyMac x"{
                    self.appExisted = existApplication(appName: "CleanMyMac X")
                }else{
                    self.appExisted = existApplication(appName: appName)
                }
            }
            
        }
        .buttonStyle(.plain)
        .sheet(isPresented: self.$authIsShowing) {
            AuthenticationView(username: .constant(""))
                .environmentObject(AFManager())
        }

    }
    
    func downloadDmgFile(dmgFileUrl: String, completion:@escaping (Bool) -> ()){
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        let url = URL(string: dmgFileUrl)!

        AF.download(
            url,
            method: .get,
            encoding: JSONEncoding.default,
            headers: nil,
            to: destination).downloadProgress(closure: { (progress) in
                print("******")
                print(progress.fractionCompleted)
                downloadProgress = progress.fractionCompleted
            }).response(completionHandler: { (DefaultDownloadResponse) in
                print(">>>>>>>")
                print(DefaultDownloadResponse.description)
                completion(true)
            })
    }
    
    func installAppOnMac(scriptFileName: String, completion:@escaping (Bool) -> ()){
        let task = Process()
        task.launchPath = "/usr/bin/osascript"
        task.arguments = ["\(NSHomeDirectory())/Documents/\(scriptFileName)"]
        task.launch()
        task.waitUntilExit()

        completion(true)
    }
    
    func openApplication(appName: String) {
           
        let task = Process()
        task.launchPath = "/usr/bin/open"
        let args = ["-n", "/Applications/\(appName).app", "--args", ""]
        
        task.arguments = args

        let pipe = Pipe()
        let errorPipe = Pipe()
        task.standardOutput = pipe
        task.standardError = errorPipe
        task.launch()
        task.waitUntilExit()

        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output = String(data: data, encoding: String.Encoding.utf8)!
        let errorData = errorPipe.fileHandleForReading.readDataToEndOfFile()
        let error = String(decoding: errorData, as: UTF8.self)

        print("out put from shell command \(output) error \(error)")
    }
    
    func existApplication(appName: String) -> Bool {
                   
        let task = Process()
        task.launchPath = "/usr/bin/env"
        let args = ["ls", "Applications"]

        task.arguments = args

        let pipe = Pipe()
        let errorPipe = Pipe()
        task.standardOutput = pipe
        task.standardError = errorPipe
        task.launch()
        task.waitUntilExit()

        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output = String(data: data, encoding: String.Encoding.utf8)!
        let errorData = errorPipe.fileHandleForReading.readDataToEndOfFile()
        let error = String(decoding: errorData, as: UTF8.self)

//        print("out put from shell command \(output) error \(error)")
        
        return output.contains(appName)
    }


    func deleteFile(fileName : String)->Bool{
        _ = FileManager.default
        let docDir = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let filePath = docDir.appendingPathComponent(fileName)
        do {
            try FileManager.default.removeItem(at: filePath)
            print("File deleted")
            return true
        }
        catch {
            print("Error")
        }
        return false
    }


}



 
 




