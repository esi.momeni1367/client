//
//  AppsListViewModel.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/4/23.
//

import Foundation
import Combine

class AppsListViewModel: ObservableObject {
    
    private let apiManager = APIManager<CategoriesApplicationsEndPoint>()
    
    private var bag = Set<AnyCancellable>()
    
    @Published private (set) var applicationList: ApplicationListViewModel?
    @Published var title: String?
    
    func getApps(catId: String, skip: Int, limit: Int) {
        
        apiManager.getData(from: .getApplications(catId: catId, skip: skip, limit: limit))
            .decode(type: ApplicationListContainer.self, decoder: Container.jsonDecoder)
            .map { $0 }
            .replaceError(with: ApplicationListContainer(items: [], total: 0, limit: 0, offset: 0))
            .receive(on: RunLoop.main)
            .sink(receiveValue: { [weak self] appList in
                self?.applicationList = ApplicationListViewModel(appList: appList)
            })
            .store(in: &bag)
    }
}
