//
//  AppsListView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/4/23.
//

import SwiftUI

struct AppsListView: View {
    
    @EnvironmentObject var appsListViewModel: AppsListViewModel
    
    @State var apps: [ApplicationViewModel] = []
    
    @Binding var currentPage: PageStatus
    @Binding var previusePage: PageStatus
    @Binding var globalData: GlobalData
    @Binding var categoryId: String
    var GreatAppTitle: String = "بهترین اپ‌ها و آپدیت‌های تازه"
    
    var body: some View {
        VStack(alignment: .trailing, spacing: 0) {
            
            if self.currentPage == .GreatAppList {
                TopButtonView(currentPage: self.$currentPage, previusePage: self.$previusePage)
                HStack {
                    Spacer()
                    Text(self.GreatAppTitle)
                        .font(.MacStore.HomePage.featuredAppMainTitle)
                        .fontWeight(.bold)
                        .padding(.horizontal)
                }
            }else if self.currentPage == .AppList {
                HStack {
                    Spacer()
                    Text(self.appsListViewModel.title ?? "")
                        .font(.MacStore.HomePage.featuredAppMainTitle)
                        .fontWeight(.bold)
                        .padding(.horizontal)
                }
            }
            
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center, spacing: 0) {
                    HStack {
                        if self.currentPage == .GreatAppList {
                            ForEach(self.apps, id:\.id) { appInfo in
                                AppCardView(app: appInfo)
                                    .onTapGesture {
                                        self.currentPage = .AppDetail
                                        self.globalData.appDetail = appInfo
                                    }
                            }
                        }else if self.currentPage == .AppList {
                            ForEach(self.appsListViewModel.applicationList?.apps ?? [], id:\.id) { appInfo in
                                AppCardView(app: appInfo)
                                    .onTapGesture {
                                        self.currentPage = .AppDetail
                                        self.globalData.appDetail = appInfo
                                    }
                            }
                        }
                    }
                }
                .padding(.horizontal)
                .padding(.bottom, 40)
            }.padding(.top)
        }
    }
}
