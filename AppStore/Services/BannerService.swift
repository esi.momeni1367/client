//
//  BannerService.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import Foundation
import Combine


class BannerService {
    
    private let apiManager = APIManager<BannersEndpoint>()

    func getBannerList(skip: Int, limit: Int) -> AnyPublisher<Data,Error> {
        return apiManager.getData(from: .getBanners(skip: skip, limit: limit))
            .eraseToAnyPublisher()
    }
}
