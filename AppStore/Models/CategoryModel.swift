//
//  CategoryModel.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import Foundation

struct CategoryContainer {
    let items : [CategoryModel]
    let total: String
    let limit: String
    let offset: String
}

struct CategoryModel {
    let version: String
    let title: String
    let desc: String
    let image_url: String
    let created_at: String
    let updated_at: String
    let _id: String
}
