//
//  SideBarModel.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import Foundation

struct SideBarContainer: Decodable {
    let version: String
    let tags: [SideBarModel]
}

struct SideBarModel: Decodable {
    let version: String
    let title: String
    let desc: String
    let image_url: String
    let show_in_sidebar: Bool
    let created_at: String
    let updated_at: String
    let _id: String
}
