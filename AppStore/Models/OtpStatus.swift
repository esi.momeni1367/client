//
//  OtpStatus.swift
//  AppStore
//
//  Created by Esmaeil on 5/13/23.
//

class OtpStatus: Codable {
    
    var version : String?
    var status : String?

    private enum CodingKeys : String, CodingKey{
        
        case version
        case status

    }
    
    init() {
    }
    
    required init (from decoder : Decoder) throws{
        let container = try! decoder.container(keyedBy: CodingKeys.self)
        
        do {
            self.version = try container.decodeIfPresent(String.self, forKey: .version) ?? ""
        }catch{
            print("Unable to decode for this type!")
        }

        do {
            self.status = try container.decodeIfPresent(String.self, forKey: .status) ?? ""
        }catch{
            print("Unable to decode for this type!")
        }
    }

}
