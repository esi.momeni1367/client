//
//  ApplicationModel.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import Foundation

struct ApplicationListContainer: Decodable {
    let items: [ApplicationModel]
    let total: Int
    let limit: Int
    let offset: Int
}


struct ApplicationModel: Decodable {
    
    let version: String
    let name: String
    let logo: String
    let desc: ApplicationDescriptionContainer
    let age: String
    let rank: String
    let developer: String
    let catalogue: [ApplicationCatalogue]
    let created_at: String
    let updated_at: String
    let _id: String
    let category: ApplicationCategory
}


struct ApplicationDescriptionContainer: Decodable {
    let short: ApplicationDescription
    let long: ApplicationDescription
}

struct ApplicationDescription: Decodable {
    let en: String
    let fa: String
}
     

struct ApplicationCategory: Decodable {
    let version: String
    let title: String
    let desc: String
    let image_url: String
    let created_at: String
    let updated_at: String
    let _id: String
}

struct ApplicationCatalogue: Decodable {
    let app_version: String
    let feeder_state: Int
    let screenshots: [String]
    let size: String
    let languages: [String]
    let compatibility: ApplicationCompatibility
    let test_datetime: String
    let publish_datetime: String
    let release_datetime: String
    let request_datetime: String
    let notes: String
}


struct ApplicationCompatibility: Decodable {
    let os: ApplicationCompatibilityOS
    let compat: [String]
}

struct ApplicationCompatibilityOS: Decodable {
    let min: Int
    let max: Int
    let arch: [String]
}

struct AppInfo: Decodable {
    
    var name: String = ""
    var version: String = ""
    var bundle: String = ""
    var script: String = ""
    
}
