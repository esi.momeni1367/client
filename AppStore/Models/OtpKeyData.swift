//
//  OtpKeyData.swift
//  AppStore
//
//  Created by Esmaeil on 5/13/23.
//

class OtpKeyData: Codable {
    var otp: OtpKeyInfo?
    
    init() {
        self.fillData()
    }
    
    private func fillData(){
        otp = OtpKeyInfo()
    }
}

class OtpKeyInfo: Codable {
    var version = "0.0.1"
    var mobile = ""
    var key = ""
}
