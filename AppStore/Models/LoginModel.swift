//
//  LoginModel.swift
//  AppStore
//
//  Created by Esmaeil on 5/8/23.
//

struct LoginModel: Decodable {
    let version: String
    let user: User?
}


struct User: Decodable {
    
    let version: String
    let username: String
    let email: String
    let mobile: Int
    
}

class ClientModel: Codable{
    var user : ClientInfo?
    
    private enum CodingKeys : String, CodingKey{
        case user
    }
    
    init() {
    }
    
    required init (from decoder : Decoder) throws{
        let container = try! decoder.container(keyedBy: CodingKeys.self)
        
        do {
            self.user = try container.decodeIfPresent(ClientInfo.self, forKey: .user) ?? ClientInfo()
        }catch{
            print("Unable to decode for this type!")
        }

    }

}


class ClientInfo: Codable {
    
    var username : String?
    var token : String?

    private enum CodingKeys : String, CodingKey{
        
        case username
        case token

    }
    
    init() {
    }
    
    required init (from decoder : Decoder) throws{
        let container = try! decoder.container(keyedBy: CodingKeys.self)
        
        do {
            self.username = try container.decodeIfPresent(String.self, forKey: .username) ?? ""
        }catch{
            print("Unable to decode for this type!")
        }
        
        do {
            self.token = try container.decodeIfPresent(String.self, forKey: .token) ?? ""
        }catch{
            print("Unable to decode for this type!")
        }
    }

}
