//
//  BannerModel.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import Foundation

struct BannerContainer: Decodable {
    let items: [BannerModel]
    let total: Int
    let limit: Int
    let offset: Int
}

struct BannerModel: Decodable {
    let version: String
    let title: String
    let slug: String
    let type: String
    let desc: String
    let image_url: String
    let tag_id: String
    let created_at: String
    let updated_at: String
    let _id: String
}
