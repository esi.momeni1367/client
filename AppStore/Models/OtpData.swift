//
//  OtpData.swift
//  AppStore
//
//  Created by Esmaeil on 5/13/23.
//

class OtpData: Codable {
    var otp: OtpInfo?
    
    init() {
        self.fillData()
    }
    
    private func fillData(){
        otp = OtpInfo()
    }
}

class OtpInfo: Codable {
    var version = "0.0.1"
    var mobile = ""
}
