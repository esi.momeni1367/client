//
//  TagModel.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import Foundation

struct TagContainer {
    let items: TagModel
    let total: Int
    let limit: Int
    let offset: Int
}


struct TagModel {
    let version: String
    let title: String
    let desc: String
    let image_url: String
    let show_in_sidebar: String
    let created_at: String
    let updated_at: String
    let _id: String
}
