//
//  AuthenticationModel.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import Foundation


struct AuthenticationContainer {
    let version: String
    let user: AuthenticationModel
}

struct AuthenticationModel {
    let version: String
    let username: String
    let email: String
    let mobile: Double
    let bio: String
    let image: String
    let phone: String
    let verified: [String]
    let roles: [String]
    let subscribed: [String]
    let app_list: [String]
    let device_list: [String]
    let token: String
}


struct OTPModel {
    let version: String
    let status: String
}
