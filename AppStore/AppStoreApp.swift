//
//  AppStoreApp.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 2/23/23.
//

import SwiftUI
import AppKit

@main
struct AppStoreApp: App {
    
    init() {
        _ = Setting.default.load()
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
        
        .windowStyle(HiddenTitleBarWindowStyle.hiddenTitleBar)
        
    }
}
