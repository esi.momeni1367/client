//
//  MFont.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/4/23.
//

import Foundation
import SwiftUI

struct FontBase {
    
    enum FontSize: CGFloat {
        case
        xXxSmall = 11,
        xXSmall = 12,
        xSmall = 13,
        small = 14,
        medium = 15,
        large = 16,
        xLarge = 18,
        xXLarge = 20,
        xXxlarge = 24
    }
    enum PersianFontFamily: String {
        case
        YekanBakh_Bold = "YekanBakh-Bold",
        YekanBakh_Medium = "YekanBakh-Medium",
        YekanBakh_Regular = "YekanBakh-Regular",
        YekanBakh_Light = "YekanBakh-Light",
        YekanBakh_Thin = "YekanBakh-Thin"
    }
    
    enum EnglishFontFamily: String {
        case
        Poppins_Bold = "Poppins-Bold",
        Poppins_Medium = "Poppins-Medium",
        Poppins_Regular = "Poppins-Regular",
        Poppins_Light = "Poppins-Light",
        Poppins_Thin = "Poppins-Thin"
    }
    
}


extension Font {
    
    struct MacStore {
        
        struct General {
            
            static let sideBarItem: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Medium.rawValue,
                        size: FontBase.FontSize.small.rawValue)
            
            static let downloadButton: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Bold.rawValue,
                        size: FontBase.FontSize.small.rawValue)
        }
        
        struct HomePage {
            
            static let seeMoreButton: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Regular.rawValue,
                        size: FontBase.FontSize.small.rawValue)
                        
            static let featuredAppMainTitle: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Bold.rawValue,
                        size: FontBase.FontSize.xXLarge.rawValue)
            
            static let featuredAppslug: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Bold.rawValue,
                        size: FontBase.FontSize.small.rawValue)
            
            static let featuredAppTitle: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Bold.rawValue,
                        size: FontBase.FontSize.xXxlarge.rawValue)
            
            static let featuredAppdescription: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Bold.rawValue,
                        size: FontBase.FontSize.medium.rawValue)
            
            static let GreatAppTitle: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Bold.rawValue,
                        size: FontBase.FontSize.xXLarge.rawValue)
            
        }
        
        
        struct AppsList {
            
            static let mainTitle: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Medium.rawValue,
                        size: FontBase.FontSize.xXLarge.rawValue)
            
            static let appTitle: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Regular.rawValue,
                        size: FontBase.FontSize.large.rawValue)
            
            
            static let appDescription: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Regular.rawValue,
                        size: FontBase.FontSize.small.rawValue)   
        }
        
        struct FilterList {
            
            static let itemTitle: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Regular.rawValue,
                        size: FontBase.FontSize.xLarge.rawValue)
            
            static let segmentControl: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Regular.rawValue,
                        size: FontBase.FontSize.medium.rawValue)
            
            static let readyButton: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Bold.rawValue,
                        size: FontBase.FontSize.xLarge.rawValue)
        }
        
        struct GalleryList {
            
            
            static let segmentControl: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Regular.rawValue,
                        size: FontBase.FontSize.large.rawValue)
        }
        
        struct Lists {
            
           
            static let itemSubtitle: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Light.rawValue,
                        size: FontBase.FontSize.xXSmall.rawValue)
            
            static let itemPrice: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Regular.rawValue,
                        size: FontBase.FontSize.small.rawValue)
        }
        
        typealias offerList = Lists
        typealias newsList = Lists
        typealias foodList = Lists
        typealias restaurantList = Lists
        
        
        struct OfferProfile {
            
          
            static let description: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Regular.rawValue,
                        size: FontBase.FontSize.large.rawValue)
            
            static let callButton: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Bold.rawValue,
                        size: FontBase.FontSize.xLarge.rawValue)
        }
        
        struct Map {
            static let directionButton: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Bold.rawValue,
                        size: FontBase.FontSize.xLarge.rawValue)
            
            static let loadMoreButton: Font = Font
                .custom(FontBase.PersianFontFamily.YekanBakh_Bold.rawValue,
                        size: FontBase.FontSize.xLarge.rawValue)
        }
        
    }
}


