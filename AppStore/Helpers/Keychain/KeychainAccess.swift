//
//  KeychainAccess.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/3/23.
//

import Foundation

enum KeychainKeys: String {
    case token
    case email
}

class KeychainAccess {
    
    static let shared = KeychainAccess()
    private init() {}
    
    func removeAll() {
        let _ = KeychainWrapper.standard.removeAllKeys()
    }
    
    var token: String {
        get {
            return KeychainWrapper.standard.string(forKey: KeychainKeys.token.rawValue) ?? ""
        }
        set (newValue) {
            KeychainWrapper.standard.set(newValue, forKey: KeychainKeys.token.rawValue)
        }
    }
    
    var email: String {
        get {
            return KeychainWrapper.standard.string(forKey: KeychainKeys.email.rawValue) ?? ""
        }
        set (newValue) {
            KeychainWrapper.standard.set(newValue, forKey: KeychainKeys.email.rawValue)
        }
    }
}
