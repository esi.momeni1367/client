//
//  GlobalData.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/3/23.
//

import Foundation

class GlobalData: ObservableObject {
    @Published var currentPage: PageStatus = .Home
    @Published var previusePage: PageStatus = .Home
    @Published var appDetail: ApplicationViewModel = ApplicationViewModel(appInfo: nil)
}

enum PageStatus {
    case Home
    case FeaturedApp
    case AppList
    case GreatAppList
    case AppDetail
    case account
}
