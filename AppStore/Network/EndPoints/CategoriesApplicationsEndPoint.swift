//
//  CategoriesApplicationsEndPoint.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/4/23.
//

import Foundation

enum CategoriesApplicationsEndPoint: EndpointProtocol {
    
    case getApplications(catId: String,skip: Int, limit: Int)
    
    var absoluteURL: String {
        switch self {
        case let .getApplications(catId, _, _):
            return baseURL + "category/\(catId)/apps"
        }
    }
    
    var params: [String: String] {
        switch self {
        
        case let .getApplications(_, skip, limit):
            return ["skip": String(skip), "limit": String(limit)]
        }
    }
    
    var headers: [String: String] {
        return [
            "Content-type": "application/json",
            "Accept": "application/json",
        ]
    }
}
