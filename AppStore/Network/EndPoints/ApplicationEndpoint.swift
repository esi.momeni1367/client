//
//  ApplicationEndpoint.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/3/23.
//

import Foundation

enum ApplicationDetailEndpoint: EndpointProtocol {
    
    case getApplication(appId: String)
    
    var absoluteURL: String {
        switch self {
        case let .getApplication(appId):
            return baseURL + "app/\(appId)/details"
        }
    }
    
    var params: [String: String] {
        switch self {
        
        case .getApplication:
            return [:]
        }
    }
    
    var headers: [String: String] {
        return [
            "Content-type": "application/json",
            "Accept": "application/json",
        ]
    }
}
