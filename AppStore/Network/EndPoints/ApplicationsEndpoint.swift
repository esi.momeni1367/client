//
//  ApplicationsEndpoint.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/3/23.
//

import Foundation

enum ApplicationsEndpoint: EndpointProtocol {
    
    case getApplications(skip: Int, limit: Int)
    
    var absoluteURL: String {
        switch self {
        case .getApplications:
            return baseURL + "app"
        }
    }
    
    var params: [String: String] {
        switch self {
        
        case let .getApplications(skip, limit):
            return ["skip": String(skip), "limit": String(limit)]
        }
    }
    
    var headers: [String: String] {
        return [
            "Content-type": "application/json",
            "Accept": "application/json",
        ]
    }
}
