//
//  BannersEndpoint.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import Foundation


enum BannersEndpoint: EndpointProtocol {
    
    case getBanners(skip: Int, limit: Int)
    
    var absoluteURL: String {
        switch self {
        case .getBanners:
            return baseURL + "banner"
        }
    }
    
    var params: [String: String] {
        switch self {
        
        case let .getBanners(skip, limit):
            return ["skip": String(skip), "limit": String(limit)]
        }
    }
    
    var headers: [String: String] {
        return [
            "Content-type": "application/json",
            "Accept": "application/json"
        ]
    }
}
