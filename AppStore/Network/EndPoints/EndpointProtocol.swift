//
//  EndpointProtocol.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import Foundation

protocol EndpointProtocol {
    var locale: String { get }
    
    var region: String { get }
    
    var absoluteURL: String { get }
    
    var params: [String: String] { get }
    
    var headers: [String: String] { get }
}

extension EndpointProtocol {
    var locale: String {
        return Locale.current.language.languageCode?.identifier ?? "en"
    }
    
    var region: String {
        return Locale.current.language.region?.identifier ?? "us"
    }
}
