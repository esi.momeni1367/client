//
//  SideBarEndpoint.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 3/2/23.
//

import Foundation

enum SideBarEndpoint: EndpointProtocol {
    
    case getSideBar
    
    var absoluteURL: String {
        switch self {
        case .getSideBar:
            return baseURL + "sidebar"
        }
    }
    
    var params: [String: String] {
        switch self {
        
        case .getSideBar:
            return [:]
        }
    }
    
    var headers: [String: String] {
        return [
            "Content-type": "application/json",
            "Accept": "application/json"
        ]
    }
}
