//
//  LoginEndPoint.swift
//  AppStore
//
//  Created by Esmaeil on 5/8/23.
//

import Foundation

enum LoginEndPoint: EndpointProtocol {
    
    case getApplications(skip: Int, limit: Int)
    
    var absoluteURL: String {
        switch self {
        case let .getApplications(_, _):
            return baseURL + "users/login"
        }
    }
    
    var params: [String: String] {
        switch self {
        
        case let .getApplications(skip, limit):
            return ["skip": String(skip), "limit": String(limit)]
        }
    }
    
    var headers: [String: String] {
        return [
            "Content-type": "application/json",
            "Accept": "application/json",
        ]
    }
}
