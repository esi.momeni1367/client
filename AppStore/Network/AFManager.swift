//
//  AFManager.swift
//  AppStore
//
//  Created by Esmaeil on 5/8/23.
//

import Foundation
import Alamofire

protocol FetchDataProtocol {
    func fetchData()
}

class AFManager: ObservableObject {
        
    func setRequest(url: URL, method: HTTPMethod = .get) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.method = method
        urlRequest.timeoutInterval = 20
        urlRequest.headers.add(.contentType("application/json"))
        urlRequest.headers.add(name: "Authorization", value: "Bearer \(Setting.default.token ?? "")")
        return urlRequest
    }

    func otp(otpData: OtpData, completion:@escaping (Bool, OtpStatus) -> ()) {
                
        let url = URL(string: "\(baseURL)users/otp-gen")!
        var urlRequest = URLRequest(url: url)
        
        urlRequest.method = .post
        urlRequest.timeoutInterval = 20
    
        do {
            urlRequest.httpBody = try JSONEncoder().encode(otpData)
        } catch {
            print(error)
        }
        
        urlRequest.headers.add(.contentType("application/json"))
        
        AF.request(urlRequest)
            .validate()
            .response(completionHandler: { response in
                switch response.result {
                case .success:
                    if let data = response.data {
                        do {
                            let info = try JSONDecoder().decode(OtpStatus.self, from: data)
                            completion(true, info)
                            
                        } catch _ as NSError {
                            completion(false, OtpStatus())
                        }
                        return
                    }
                    
                case .failure(_):
                    completion(false, OtpStatus())
                }
            })
    }
    
    func login(otpKeyData: OtpKeyData, completion:@escaping (Bool, ClientInfo) -> ()) {
                
        let url = URL(string: "\(baseURL)users/otp")!
        var urlRequest = URLRequest(url: url)
        
        urlRequest.method = .post
        urlRequest.timeoutInterval = 20
    
        do {
            urlRequest.httpBody = try JSONEncoder().encode(otpKeyData)
        } catch {
            print(error)
        }
        
        urlRequest.headers.add(.contentType("application/json"))
        
        AF.request(urlRequest)
            .validate()
            .response(completionHandler: { response in
                switch response.result {
                case .success:
                    if let data = response.data {
                        do {
                            let info = try JSONDecoder().decode(ClientModel.self, from: data)
                            completion(true, info.user ?? ClientInfo())
                            
                        } catch _ as NSError {
                            completion(false, ClientInfo())
                        }
                        return
                    }
                    
                case .failure(_):
                    completion(false, ClientInfo())
                }
            })
    }
    
    func getAppInfo(appId: String, completion:@escaping (Bool, AppInfo) -> ()){
        let url = URL(string: "\(baseURL)app/\(appId)/v4.10.6/download")!
        let urlRequest = setRequest(url: url)
        
        AF.request(urlRequest)
            .validate()
            .response(completionHandler: { response in
                switch response.result {
                case .success:
                    if let data = response.data {
                        do {
                            let info = try JSONDecoder().decode(AppInfo.self, from: data)
                            completion(true, info)
                            
                        } catch _ as NSError {
                            completion(false, AppInfo())
                        }
                        return
                    }
                case .failure(_):
                    completion(false, AppInfo())
                }
            })
    }
    
    func downloadScriptFile(scriptFileUrl: String, completion:@escaping (Bool) -> ()){
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        let url = URL(string: scriptFileUrl)!

        AF.download(
            url,
            method: .get,
            encoding: JSONEncoding.default,
            headers: nil,
            to: destination).downloadProgress(closure: { (progress) in
//                print("******")
//                print(progress)
            }).response(completionHandler: { (DefaultDownloadResponse) in
//                print(">>>>>>>")
//                print(DefaultDownloadResponse.description)
                completion(true)
            })
    }
    
    
//    func fetchTokenInfo(username: String, password: String, completion:@escaping (Bool, TokenInfo) -> ()) {
//
//        let parameters = [
//            "grant_type" : Constants.GRANT_TYPE,
//            "client_id" : Constants.CLIENT_ID,
//            "client_secret" : Constants.CLIENT_SECRET,
//            "username" : username,
//            "password" : password,
//            "scope" : ""
//        ]
//        let url = URL(string: "\(Constants.BASE_URL)\(Constants.GET_TOKEN_URL)")!
//        var urlRequest = URLRequest(url: url)
//        urlRequest.method = .post
//        urlRequest.timeoutInterval = 10
//
//        do {
//            urlRequest.httpBody = try JSONEncoder().encode(parameters)
//        } catch {
//            print(error)
//        }
//
//        urlRequest.headers.add(.contentType("application/json"))
//
//        AF.request(urlRequest)
//            .validate()
//            .response(completionHandler: { response in
//                switch response.result {
//                case .success:
//                    if let data = response.data {
//                        do {
//                            let info = try JSONDecoder().decode(TokenInfo.self, from: data)
//                            completion(true, info)
//
//                        } catch _ as NSError {
//                            completion(false, TokenInfo())
//                        }
//                        return
//                    }
//
//                case .failure(_):
//                    completion(false, TokenInfo())
//                }
//            })
//    }
    
}


