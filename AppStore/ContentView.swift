//
//  ContentView.swift
//  AppStore
//
//  Created by Hassan Sadegh Moghaddasi on 2/23/23.
//

import SwiftUI

struct ContentView: View {
        
    
    @ObservedObject var bannersViewModel = BannersViewModel()
    @ObservedObject var appsViewModel = ApplicationsViewModel()
    @ObservedObject var sideBarViewModel = SideBarViewModel()
    @ObservedObject var appsListViewModel = AppsListViewModel()

    @State var currentPage: PageStatus = .Home
    @State var previusePage: PageStatus = .Home
    @State var globalData: GlobalData = GlobalData()
    @State var SelectedSideBarId: String = ""
    @State var SideBarChanged: Bool = false
    
    var home: some View {
        return HomeView(currentPage: self.$currentPage, previusePage: self.$previusePage, globalData: self.$globalData)
    }
    
    var featuredApp: some View {
        return HomeView(currentPage: self.$currentPage, previusePage: self.$previusePage, globalData: self.$globalData)
    }
    
    var appList: some View {
        return AppsListView(currentPage: self.$currentPage, previusePage: self.$previusePage, globalData: self.$globalData, categoryId: self.$SelectedSideBarId)
    }
    
    var greatAppList: some View {
        return AppsListView(apps: self.appsViewModel.applicationList?.apps ?? [], currentPage: self.$currentPage, previusePage: self.$previusePage, globalData: self.$globalData, categoryId: self.$SelectedSideBarId)
    }
    
    var appDetail: some View {
        ApplicationDetailView(currentPage: self.$currentPage, previusePage: self.$previusePage, globalData: self.$globalData)
    }
    
    var sideBar: some View {
        return SideBarView(currentPage: self.$currentPage, previusePage: self.$previusePage)
    }
    
    var account: some View {
        return AccountView(apps: self.appsViewModel.applicationList?.apps ?? [], currentPage: self.$currentPage, previusePage: self.$previusePage, globalData: self.$globalData)
    }
    
    
    var body: some View {
        
        NavigationStack {
            HStack {
                switch self.currentPage {
                case .Home:
                    self.home
                        .environmentObject(self.bannersViewModel)
                        .environmentObject(self.appsViewModel)
                    
                case .FeaturedApp:
                    self.featuredApp
                    
                case .AppList:
                    self.appList
                        .environmentObject(self.appsListViewModel)
                
                case .GreatAppList:
                    self.greatAppList
                        
                
                case.AppDetail:
                    self.appDetail
                    
                case .account:
                    self.account
                }
                
                self.sideBar
                    .frame(width: 250)
                    .environmentObject(self.sideBarViewModel)
                
            }
        }
        .onAppear {
            if !(self.bannersViewModel.banner?.items.count ?? 0 > 0) {
                self.bannersViewModel.getBanners(skip: 0, limit: 10)
            }
            if !(self.appsViewModel.applicationList?.apps.count ?? 0 > 0) {
                self.appsViewModel.getApps(skip: 0, limit: 10)
            }
            if !(self.sideBarViewModel.sidebarList?.count ?? 0 > 0) {
                self.sideBarViewModel.getSideBar()
            }
        }
        
        .onReceive(self.sideBarViewModel.$currentSideBarId) { id in
            if !(id.isEmpty) {
                self.appsListViewModel.title = self.sideBarViewModel.sidebarList?.first(where: {$0.id == id })?.title
                self.appsListViewModel.getApps(catId: id, skip: 0, limit: 20)
            }
        }
        
        .onReceive(NotificationCenter.default.publisher(for: NSApplication.didBecomeActiveNotification), perform: { _ in
            NSApp.mainWindow?.standardWindowButton(.zoomButton)?.isHidden = true
            NSApp.mainWindow?.standardWindowButton(.closeButton)?.isHidden = true
            NSApp.mainWindow?.standardWindowButton(.miniaturizeButton)?.isHidden = true
            
        })
        
    }
}

